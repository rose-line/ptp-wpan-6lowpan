# Precision Time Protocol over WPAN & 6LoWPAN #

Precision Time Protocol (PTP) is designed for Ethernet Network Interface Cards (NICs). Ethernet NIC is exposed as a PTP clock (/dev/ptpX) and whenever the ethernet NIC transmits or receives packets, these packets are timestamped using this PTP clock. These timestamps are generated in the hardware, thus they are very precise and provide good accuracy for time synchronization.

This repository aims to leverage the existing PTP abstraction to provide precise timing support to wireless radio by doing the following,

* Enable Linux socket based hardware timestamping for IEEE 802.15.4 WPAN interface and 6LoWPAN interface
* Expose the IEEE 802.15.4 transceiver as a PTP clock (/dev/ptp1)
* Use Linuxptp to do time synchronization for multple nodes over WPAN and 6LoWPAN interface
* For testing, we use ultrawideband IEEE 802.15.4 transceiver DW1000 interfaced with Beaglebone Black through SPI. Multiple DW1000 (/dev/ptp1) clocks are time synchronized.

# License #

Copyright (c) Regents of the University of California, 2015.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

# Installation and Usage #

Please refer to the [wiki](https://bitbucket.org/rose-line/ptp-wpan-6lowpan/wiki).

# Contributors #

**University of California Los Angeles (UCLA)**

* Fatima Anwar

Questions can be directed to, fatimanwar@ucla.edu