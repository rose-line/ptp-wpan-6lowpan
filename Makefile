SHELL := /bin/bash

obj-m += dw1000.o

KERNELDIR ?= /export/bb-kernel/KERNEL
KERNELVER ?= 4.1.12-bone-rt-r16

all:
	dtc -O dtb -o dts/DW1000-00A0.dtbo -b 0 -@ dts/DW1000-00A0.dts

clean:
	rm dts/*.dtbo

install:
	## /export/rootfs is the path to the NFS mounted beaglebone black nodes ##
	sudo cp -v 4.1.12-bb-kernel/modules/dw1000/*.ko /export/rootfs/lib/modules/$(KERNELVER)/kernel/drivers/net/ieee802154/
	sudo cp -v dts/*.dtbo /export/rootfs/lib/firmware
	sudo cp 4.1.12-bb-kernel/modules/dw1000/80-qot.rules /export/rootfs/etc/udev/rules.d/
	sudo cp dts/capes /export/rootfs/usr/bin/
	sudo chmod 755 /export/rootfs/usr/bin/capes