/*
 * DW1000 driver
 *
 * Copyright (C) 2015 University of California Los Angeles
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Written by:
 * Andrew Symington <andrew.c.symington@gmail.com>
 */

#include <linux/types.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/spi/spi.h>
#include <linux/interrupt.h>
#include <linux/skbuff.h>
#include <linux/of_gpio.h>
#include <linux/ieee802154.h>

/* Periodic timer for overflow checking on the decawave */
#include <linux/interrupt.h>
#include <linux/hrtimer.h>
#include <linux/sched.h>

/* 64 bit division */
#include <linux/math64.h>

/* Hardware timestamping support */
#include <linux/etherdevice.h>
#include <linux/netdevice.h>
#include <linux/net_tstamp.h>

/* MAC abstraction layer */
#include <net/mac802154.h>
#include <net/cfg802154.h>

/* PTP abstraction layer */
#include <linux/clk.h>
#include <linux/clkdev.h>
#include <linux/clocksource.h>
#include <linux/device.h>
#include <linux/list.h>
#include <linux/ptp_clock_kernel.h>
#include <linux/timecounter.h>

/* Defines and register set */
#include "deca_device_api.h"

// SPI config
#define SPI_BITS_PER_WORD 				(8)
#define SPI_DELAY_USECS   				(0)
#define SPI_SPEED_SLOW    				( 3000000)
#define SPI_SPEED_FAST  	  			(10000000)

// max range estimates to keep in memory
#define DWR_MAX_RANGE_HISTORY 			(32)

// Superframe period, governing master beacons
#define DWR_SUPERFRAME_PERIOD_MS        (1000)

// Frames at 10 Hz during which each tag gets position estimate
#define DWR_FRAME_PERIOD_MS             (100)

// Subframes in each frame dedicated to a single tag
#define DWR_SUBFRAME_MAXQUERIES         (10)
#define DWR_SUBFRAME_PERIOD_MS 			(DWR_FRAME_PERIOD_MS/DWR_SUBFRAME_MAX)
#define DWR_SUBFRAME_MAXRESPONSES       (16)

// Transmission timing and dithering
#define DWR_RX_TIMEOUT_MS            	(50)
#define DWR_REPLY_DELAY_US           	(1000)
#define DWR_RANGE_INIT_MAX_DITHER_MS 	(50)
#define DWR_RANGE_RESP_MAX_DITHER_MS 	(6)

// DW High frequency timer units
#define DWR_TIMER_UNITS_HI_NS 			(4)
#define DWR_TIMER_UNITS_LO_PS 			(15)

// Return types
#define DWR_RETURN_OK    				(0)
#define DWR_RETURN_ERR   				(-1)

// Propagation constants
#define SPEED_OF_LIGHT      			(299702547.0)

// Overflow check seconds
#define DWR_OVERFLOW_CHECK_SEC			(4)

// Typical RF constants
#define DWT_PRF_64M_RFDLY   			(514.462f)
#define DWT_PRF_16M_RFDLY   			(513.9067f)

// Frame byte lengths
#define STANDARD_FRAME_SIZE   			(32)
#define FRAME_CRC             			(2)
#define FRAME_AND_ADDR_DATA   			(9)
#define FRAME_AND_ADDR_BEACON 			(7)
#define MAX_PAYLOAD_DATA   	  			(STANDARD_FRAME_SIZE - FRAME_AND_ADDR_DATA - FRAME_CRC)

// Max SPI buffer
#define DW1000_HEAD_MAX_BUF				(3)
#define DW1000_BODY_MAX_BUF				(1024)

// Precision shift
#define DW1000_SHIFT                    (6)

/* Forward dclaration of private data */
struct dw1000_private;

/* GPIO given to use fromt he device tree */
struct dw1000_platform_data {
	int reset;
};

/* Structure to help keep track of async SPI transactions */
struct dw1000_async {
	struct dw1000_private *lp;			/* Private data */
	struct hrtimer timer;				/* High res timer for timeouts, i presume */
	struct spi_message msg;				/* The message */
	struct spi_transfer trx[2];			/* Always two transactions for DW1000*/
	uint8_t head[DW1000_HEAD_MAX_BUF];	/* Data buffer */
	uint8_t body[DW1000_BODY_MAX_BUF];	/* Data buffer */
	spinlock_t lock;					/* Prevents race conditions on head/body */
	void (*complete)(void *context);	/* Chains a spin_unlock btween callback */
	int irq;							/* Cached IRQ state */
};

/* Driver private information */
struct dw1000_private {

	struct spi_device *spi;				/* SPI device structure */
	uint32_t speed;						/* SPI speed (depends on DW1000 clock config) */
	struct dw1000_async state;			/* For async read and writes */
	struct dw1000_async async;			/* For async read and writes */
	struct ieee802154_hw *hw;			/* IEEE-802.15.4 device */
	int hwts_tx_state;					/* Enable TX timestamping */
	int hwts_rx_state;					/* Enable RX timestamping */
	struct sk_buff *skb;				/* For retro-TX manip */

	uint64_t basetime;					/* overflow bits for the decawave counter */
	uint64_t lasttime;					/* overflow bits for the decawave counter */
	struct hrtimer htimer;				/* Timer for overflow management */
	ktime_t period;						/* Timer period */

	struct ptp_clock_info info;			/* PTP gettime, settime, adjtime, adjfreq */
	struct ptp_clock *clock;			/* PTP clock handle */
	struct cyclecounter cc;				/* Linux cyclecounter */
	struct timecounter tc;				/* Linux timecounter */
	int phc_index;						/* PHC index */
	struct clk *refclk;					/* Reference clock */
	spinlock_t lock; 					/* Protects time registers */
	uint32_t cc_mult; 					/* For the nominal frequency */
};

// These global variables can be set with command line arguments when you insmod
static struct dw1000_private *priv = NULL;

// TIME FUNCTIONS //////////////////////////////////////////////////////////////

/*
static uint64_t 
dw1000_timeconv(uint64_t dwt)
{	
	// Take care of the decawave overflows
	if (dwt < priv->lasttime)
		priv->basetime += 1;
	priv->lasttime = dwt;
	return div64_u64((priv->basetime << 40) | dwt, 1e5) * 1565;

    ns = (u64) cycles;
    ns = (ns * cc->mult) + *frac;
  	*frac = ns & mask;
 	return ns >> cc->shift;
}
*/

// SPI FUNCTIONS ///////////////////////////////////////////////////////////////

static enum hrtimer_restart 
dw1000_async_state_timer(struct hrtimer *timer)
{
	//struct dw1000_async *ctx = container_of(timer, struct dw1000_async, timer);
	//dw1000_complete(ctx);
	return HRTIMER_NORESTART;
}

static void
dw1000_spi_register(struct dw1000_private *lp)
{
	/* General asyncrhonous messages */
	memset(&lp->async.trx, 0, sizeof(lp->async.trx));
	lp->async.lp = lp;
	lp->async.irq = lp->spi->irq;
	spi_message_init(&lp->async.msg);
	lp->async.msg.context = &lp->async;
	lp->async.msg.complete = NULL;
	lp->async.trx[0].bits_per_word = SPI_BITS_PER_WORD;
	lp->async.trx[0].delay_usecs = SPI_DELAY_USECS;
	lp->async.trx[0].speed_hz = lp->speed;
	lp->async.trx[0].cs_change = 0;
	lp->async.trx[1].bits_per_word = SPI_BITS_PER_WORD;
	lp->async.trx[1].delay_usecs = SPI_DELAY_USECS;
	lp->async.trx[1].speed_hz = lp->speed;
	lp->async.trx[1].cs_change = 1;
	spi_message_add_tail(&lp->async.trx[0], &lp->async.msg);
	spi_message_add_tail(&lp->async.trx[1], &lp->async.msg);
	hrtimer_init(&lp->async.timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
	lp->async.timer.function = dw1000_async_state_timer;

	/* General asyncrhonous messages */
	memset(&lp->state.trx, 0, sizeof(lp->state.trx));
	lp->state.lp = lp;
	lp->state.irq = lp->spi->irq;
	spi_message_init(&lp->state.msg);
	lp->state.msg.context = &lp->state;
	lp->state.msg.complete = NULL;
	lp->state.trx[0].bits_per_word = SPI_BITS_PER_WORD;
	lp->state.trx[0].delay_usecs = SPI_DELAY_USECS;
	lp->state.trx[0].speed_hz = lp->speed;
	lp->state.trx[0].cs_change = 0;
	lp->state.trx[1].bits_per_word = SPI_BITS_PER_WORD;
	lp->state.trx[1].delay_usecs = SPI_DELAY_USECS;
	lp->state.trx[1].speed_hz = lp->speed;
	lp->state.trx[1].cs_change = 1;
	/* Special case : CLK manipulation must be done at low rate*/
	spi_message_add_tail(&lp->state.trx[0], &lp->state.msg);
	spi_message_add_tail(&lp->state.trx[1], &lp->state.msg);
	hrtimer_init(&lp->state.timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
	lp->state.timer.function = dw1000_async_state_timer;
}

static void
dw1000_spi_unregister(struct dw1000_private *lp)
{
	hrtimer_cancel(&lp->async.timer);
	hrtimer_cancel(&lp->state.timer);
}

static int 
dw1000_readfromdevice(struct dw1000_async *ctx, 
	uint16_t recordNumber, uint16_t index, uint32_t len)
{
	int status, cnt = 0;
	if (len > DW1000_BODY_MAX_BUF)
	{
		pr_info("dw1000_readfromdevice: error\n");
		return DWT_ERROR;
	}
    if (index == 0)
        ctx->head[cnt++] = (uint8_t) recordNumber;
    else
    {
        ctx->head[cnt++] = (uint8_t)(0x40 | recordNumber);
        if (index <= 127)
            ctx->head[cnt++] = (uint8_t) index;
        else
        {
            ctx->head[cnt++] = 0x80 | (uint8_t)(index);
            ctx->head[cnt++] =  (uint8_t) (index >> 7);
        }
    }
	ctx->trx[0].len = cnt;
	ctx->trx[0].rx_buf = NULL;
	ctx->trx[0].tx_buf = ctx->head;
	ctx->trx[1].len = len;
	ctx->trx[1].rx_buf = ctx->body;
	ctx->trx[1].tx_buf = NULL;
	status = spi_async(ctx->lp->spi, &ctx->msg);
	return (status ? DWT_ERROR : DWT_SUCCESS);	
}

// EXPOSED FUNCTIONS TO THE DW FRAMEWORK ///////////////////////////////////////

int 
dw1000_write(	uint16_t headLen,   const uint8_t* head, 
				uint32_t bodyLen,   const uint8_t* body)
{
	int status;
	struct spi_message msg;				/* The message */
	struct spi_transfer trx[2];			/* Always two transactions for DW1000*/
	if (!priv || bodyLen > DW1000_BODY_MAX_BUF || headLen > DW1000_HEAD_MAX_BUF)
	{
		pr_info("dw1000_write: error\n");
		return DWT_ERROR;
	}
	memset(&trx, 0, sizeof(trx));
	trx[0].bits_per_word = SPI_BITS_PER_WORD;
	trx[0].delay_usecs = SPI_DELAY_USECS;
	trx[0].speed_hz = priv->speed;
	trx[0].cs_change = 0;
	trx[0].len = headLen;
	trx[0].rx_buf = NULL;
	trx[0].tx_buf = head;
	trx[1].bits_per_word = SPI_BITS_PER_WORD;
	trx[1].delay_usecs = SPI_DELAY_USECS;
	trx[1].speed_hz = priv->speed;
	trx[1].cs_change = 1;
	trx[1].len = bodyLen;
	trx[1].rx_buf = NULL;
	trx[1].tx_buf = body;	
	spi_message_init(&msg);
	spi_message_add_tail(&trx[0], &msg);
	spi_message_add_tail(&trx[1], &msg);
	status = spi_sync(priv->spi, &msg);
	return (status ? DWT_ERROR : DWT_SUCCESS);	
}

int 
dw1000_read(uint16_t headLen, const uint8_t* head, 
	        uint32_t bodyLen,       uint8_t* body)
{
	int status;
	struct spi_message msg;				/* The message */
	struct spi_transfer trx[2];			/* Always two transactions for DW1000*/
	if (!priv || bodyLen > DW1000_BODY_MAX_BUF || headLen > DW1000_HEAD_MAX_BUF)
	{
		pr_info("dw1000_read: error\n");
		return DWT_ERROR;
	}
	memset(&trx, 0, sizeof(trx));
	trx[0].bits_per_word = SPI_BITS_PER_WORD;
	trx[0].delay_usecs = SPI_DELAY_USECS;
	trx[0].speed_hz = priv->speed;
	trx[0].cs_change = 0;
	trx[0].len = headLen;
	trx[0].rx_buf = NULL;
	trx[0].tx_buf = head;
	trx[1].bits_per_word = SPI_BITS_PER_WORD;
	trx[1].delay_usecs = SPI_DELAY_USECS;
	trx[1].speed_hz = priv->speed;
	trx[1].cs_change = 1;
	trx[1].len = bodyLen;
	trx[1].rx_buf = body;
	trx[1].tx_buf = NULL;	
	spi_message_init(&msg);
	spi_message_add_tail(&trx[0], &msg);
	spi_message_add_tail(&trx[1], &msg);
	status = spi_sync(priv->spi, &msg);
	return (status ? DWT_ERROR : DWT_SUCCESS);	
}

void 
dw1000_sleep_ms(uint32_t tmsec)
{
	msleep(tmsec);
}


// PTP support /////////////////////////////////////////////////////////////////

static int 
dw1000_ptp_adjfreq(struct ptp_clock_info *ptp, int32_t ppb)
{
	uint64_t adj;
	uint32_t diff, mult;
	int neg_adj = 0;
	unsigned long flags;
	struct dw1000_private *priv = container_of(ptp, struct dw1000_private, info);
	if (ppb < 0) {
		neg_adj = 1;
		ppb = -ppb;
	}
	mult = priv->cc_mult;
	adj = mult;
	adj *= ppb;
	diff = div_u64(adj, 1000000000ULL);
	spin_lock_irqsave(&priv->lock, flags);
	timecounter_read(&priv->tc);
	priv->cc.mult = neg_adj ? mult - diff : mult + diff;
	spin_unlock_irqrestore(&priv->lock, flags);
	return 0;
}

static int 
dw1000_ptp_adjtime(struct ptp_clock_info *ptp, s64 delta)
{
	unsigned long flags;
	struct dw1000_private *priv = container_of(ptp, struct dw1000_private, info);
	spin_lock_irqsave(&priv->lock, flags);
	timecounter_adjtime(&priv->tc, delta);
	spin_unlock_irqrestore(&priv->lock, flags);
	return 0;
}

static int 
dw1000_ptp_gettime(struct ptp_clock_info *ptp, struct timespec64 *ts)
{
	uint64_t ns;
	unsigned long flags;
	struct dw1000_private *priv = container_of(ptp, struct dw1000_private, info);
	spin_lock_irqsave(&priv->lock, flags);
	ns = timecounter_read(&priv->tc);
	spin_unlock_irqrestore(&priv->lock, flags);
	*ts = ns_to_timespec64(ns);
	return 0;
}

static int 
dw1000_ptp_settime(struct ptp_clock_info *ptp, const struct timespec64 *ts)
{
	uint64_t ns;
	unsigned long flags;
	struct dw1000_private *priv = container_of(ptp, struct dw1000_private, info);
	ns = timespec64_to_ns(ts);
	spin_lock_irqsave(&priv->lock, flags);
	timecounter_init(&priv->tc, &priv->cc, ns);
	spin_unlock_irqrestore(&priv->lock, flags);
	return 0;
}

static int 
dw1000_ptp_enable(struct ptp_clock_info *ptp, struct ptp_clock_request *rq, int on)
{
	return -EOPNOTSUPP;
}

static struct ptp_clock_info dw1000_ptp_info = {
	.owner		= THIS_MODULE,
	.name		= "DW1000 timer",
	.max_adj	= 1000000,
	.n_alarm    = 0,
	.n_ext_ts	= 0,
	.n_pins		= 0,
	.pps		= 0,
	.adjfreq	= dw1000_ptp_adjfreq,
	.adjtime	= dw1000_ptp_adjtime,
	.gettime64	= dw1000_ptp_gettime,
	.settime64	= dw1000_ptp_settime,
	.enable		= dw1000_ptp_enable,
};

static cycle_t 
dw1000_ptp_timequery(const struct cyclecounter *cc)
{
	uint8_t tr[5];
	uint64_t ts;
	dwt_readsystime(tr);
	ts 	= ((uint64_t)tr[4] << 32) 
		+ ((uint64_t)tr[3] << 24) 
		+ ((uint64_t)tr[2] << 16) 
		+ ((uint64_t)tr[1] <<  8) 
	    + ((uint64_t)tr[0]);
	ts = (ts >> DW1000_SHIFT);
	return ts;
}

static int 
dw1000_ptp_register(struct dw1000_private *priv)
{
	uint32_t mult, shift;
	int err;
	unsigned long flags;

	priv->info = dw1000_ptp_info;
	priv->clock = ptp_clock_register(&priv->info, &priv->spi->dev);
	if (IS_ERR(priv->clock)) {
		err = PTR_ERR(priv->clock);
		priv->clock = NULL;
		return err;
	}
	spin_lock_init(&priv->lock);

	// 2^40 / 63.8976 GHz = 17.207401026 sec (Actual decawave overflow)
	// But last 6 bits of counter is zero which leaves us with 34 bit counter
	// Hence Mask of 34 bits, 2^34 / 17.207401026 sec = 998,400,000 Hz
	// NOw, calculate the shift from  998.4 MHz to 1 GHz for 17.2074s overflow range
	// Mask of 31 bits = 124.8MHz clock
	uint64_t tmp;
	uint32_t sft, sftacc = 32;
	uint64_t to   =  1000000000;	//     1 GHz = nanosecond increment
	uint64_t from =   998400000;	// 998.4 Mhz = 34 bit clock
	uint64_t secs =          18;	//    18 sec = 17.207401026s overflow 
	tmp = ((uint64_t) secs * from) >> 32;
	while (tmp) {
		tmp >>=1;
		sftacc--;
	}
	for (sft = 32; sft > 0; sft--) {
		tmp = (uint64_t) to << sft;
		tmp += from / 2;
		do_div(tmp, from);
		if ((tmp >> sftacc) == 0)
			break;
	}
	mult = tmp;
	shift = sft;
	pr_info("Calculated mult and shift values of %x and %x respectively\n",mult,shift);

	// Setup the time counter
	priv->cc.read = dw1000_ptp_timequery;
	priv->cc.mask = CLOCKSOURCE_MASK(40 - DW1000_SHIFT);
	priv->cc_mult = mult;
	priv->cc.mult = mult;
	priv->cc.shift = shift;

	// Initialize the timecoutner
	spin_lock_irqsave(&priv->lock, flags);
	timecounter_init(&priv->tc, &priv->cc, ktime_to_ns(ktime_get_real()));
	spin_unlock_irqrestore(&priv->lock, flags);

	// Set the PHC index of the clock (for ethtool to grab)
	priv->phc_index = ptp_clock_index(priv->clock);

	return 0;
}

static void 
dw1000_ptp_unregister(struct dw1000_private *priv)
{
	if (priv->clock)
		ptp_clock_unregister(priv->clock);
}


// MAC/CFG 802.15.4 FUNCTIONS //////////////////////////////////////////////////

static int 
dw1000_start(struct ieee802154_hw *hw)
{
	dwt_rxenable(0);
    return DWT_SUCCESS;
}

static void 
dw1000_stop(struct ieee802154_hw *hw)
{
	dwt_forcetrxoff();
	dwt_rxreset();
	return;
}

static int
dw1000_tx(struct ieee802154_hw *hw, struct sk_buff *skb)
{
	priv->skb = skb;
	if (priv->hwts_tx_state && (skb_shinfo(skb)->tx_flags & SKBTX_HW_TSTAMP))
		skb_shinfo(skb)->tx_flags |= SKBTX_IN_PROGRESS;
	skb_tx_timestamp(skb);
	dwt_writetxdata(skb->len + 2, skb->data, 0);
	dwt_writetxfctrl(skb->len + 2, 0);
	dwt_forcetrxoff();
	dwt_starttx(DWT_START_TX_IMMEDIATE | DWT_RESPONSE_EXPECTED);
	return DWT_SUCCESS;
}

static int
dw1000_ed(struct ieee802154_hw *hw, u8 *level)
{
	uint32_t buff, i;
	uint32_t edg1, edv2;
	int32_t fac;

    /* Extract the ED factors */
    buff = AGC_STAT1_MASK & dwt_read32bitoffsetreg(AGC_CTRL_ID, AGC_STAT1_OFFSET);
    edg1 = (buff & AGC_STAT1_EDG1_MASK) >> 6;
    edv2 = (buff & AGC_STAT1_EDG2_MASK) >> 11;

    /* Silly way to do exponents... i am ashamed */
    fac = edv2 - 40;
    for (i = 0; i < edg1; i++)
    	fac *= 10;

    /* Energy density calculation depends on channel */
    switch (hw->phy->current_channel)
    {
	case 1 : case 2: case 3: case 4:
		fac = fac * 4 / 3;
		break;
	case 5 : case 7:
		fac = fac;
		break;
	default:
		pr_info("unexpected channel in ed calculation\n");
		break;
    }

	pr_info("%u %u %d\n",edg1,edv2,fac);

    // Set the level
    *level = (uint8_t) fac;

    // Success
	return DWT_SUCCESS;
}

static int
dw1000_set_channel(struct ieee802154_hw *hw, u8 page, u8 channel)
{
	dwt_config_t ch_config;
	dwt_txconfig_t tx_calib;

	// Supported channels ony
	BUG_ON(page != 4);
	BUG_ON(channel < 1 || channel > 7 || channel == 6);
    
	///////////////////////////////////////////////////////////////////
	// CHANNEL | FREQUENCY | BANDWIDTH | 16 MHz PRF | 64 MHz PRF     //
	// CH 1    | 3494.4    | 499.2     | 1, 2       | 9,  10, 11, 12 //
	// CH 2    | 3993.6    | 499.2     | 3, 4       | 9,  10, 11, 12 //
	// CH 3    | 4492.8    | 499.2     | 5, 6       | 9,  10, 11, 12 //
	// CH 4    | 3993.6    | 1331.2*   | 7, 8       | 17, 18, 19, 20 //
	// CH 5    | 6489.6    | 499.2     | 3, 4       | 9,  10, 11, 12 //
	// CH 7    | 6489.6    | 1081.6*   | 7, 8       | 17, 18, 19, 20 //
	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////
	// Data Rate | Recommended preamble sequence length 			 //
	// 6.8 Mbps  | 64 or 128 or 256 								 //
	// 850 kbps  | 256 or 512 or 1024								 //
	// 110kbps 	 | 2048 or 4096 									 //
	///////////////////////////////////////////////////////////////////

	// default channel configuration (taken from mode 3 of EVK1000)
	ch_config.chan = 2;
	ch_config.rxCode = 9;
	ch_config.txCode = 9;
	ch_config.prf = DWT_PRF_64M;
	ch_config.dataRate = DWT_BR_6M8;
	ch_config.txPreambLength = DWT_PLEN_128;
	ch_config.rxPAC = DWT_PAC8;
	ch_config.nsSFD = 0;
	ch_config.phrMode = DWT_PHRMODE_STD;
	ch_config.sfdTO = (256+64-32);
	ch_config.smartPowerEn = 0;
	tx_calib.PGdly = 0xC2;
	tx_calib.power = 0x00000000;

	// Fixed PRF and PREAMBLE code for the given channel
	switch (channel)
	{
	case 1: 
		ch_config.chan = 1;
		tx_calib.PGdly = TC_PGDELAY_CH1;
		ch_config.txCode = 9; 	
		ch_config.rxCode = 9; 	
		break;
	case 2: 
		ch_config.chan = 2;
		tx_calib.PGdly = TC_PGDELAY_CH2;
		ch_config.txCode = 9; 	
		ch_config.rxCode = 9; 	
		break;
	case 3: 
		ch_config.chan = 3;
		tx_calib.PGdly = TC_PGDELAY_CH3;
		ch_config.txCode = 9; 	
		ch_config.rxCode = 9; 	
		break;
	case 4: 
		ch_config.chan = 4;
		tx_calib.PGdly = TC_PGDELAY_CH4;
		ch_config.txCode = 17; 	
		ch_config.rxCode = 17; 	
		break;
	case 5: 
		ch_config.chan = 5;
		tx_calib.PGdly = TC_PGDELAY_CH5;
		ch_config.txCode = 9; 	
		ch_config.rxCode = 9; 	
		break;
	case 7: 
		ch_config.chan = 7;
		tx_calib.PGdly = TC_PGDELAY_CH7;
		ch_config.txCode = 17; 	
		ch_config.rxCode = 17; 	
		break;
	default:
		pr_info("unexpected channel in ed calculation\n");
		break;
	}

	// configure the channel
	dwt_configure(&ch_config, DWT_LOADNONE);
	dwt_configuretxrf(&tx_calib);

	// success
    return DWT_SUCCESS;
}

static int
dw1000_filter(struct ieee802154_hw *hw,
	      struct ieee802154_hw_addr_filt *filt, unsigned long changed)
{
	if (changed & IEEE802154_AFILT_PANID_CHANGED) {
		uint16_t panid = le16_to_cpu(filt->pan_id);
	    dwt_setpanid(panid);
	}

	if (changed & IEEE802154_AFILT_IEEEADDR_CHANGED) {
		uint8_t* addr = (uint8_t*) &filt->ieee_addr;
 		dwt_seteui(addr);
	}

	if (changed & IEEE802154_AFILT_SADDR_CHANGED) {
		uint16_t addr = le16_to_cpu(filt->short_addr);
		dwt_setaddress16(addr);
	}

	if (changed & IEEE802154_AFILT_PANC_CHANGED) {
	    if (filt->pan_coord)
			dwt_enableframefilter(DWT_FF_COORD_EN);
	}
	return DWT_SUCCESS;
}

static int
dw1000_txpower(struct ieee802154_hw *hw, s8 dbm)
{
	uint32_t reg;
	uint32_t txpower = 0;
	reg = 0;
		 if (dbm >= 18) reg = 0b00000000;  /* 000 = 18 dB gain 	*/
	else if (dbm >= 15) reg = 0b00100000;  /* 001 = 15 dB gain 	*/
	else if (dbm >= 12) reg = 0b01000000;  /* 010 = 12 dB gain 	*/
	else if (dbm >= 9)  reg = 0b01100000;  /* 011 = 9 dB gain 	*/
	else if (dbm >= 6)  reg = 0b10000000;  /* 100 = 6 dB gain 	*/
	else if (dbm >= 3)  reg = 0b10100000;  /* 101 = 3 dB gain 	*/
	else if (dbm >= 0)  reg = 0b11000000;  /* 110 = 0 dB gain 	*/
	txpower = (reg << 16) | (reg << 8);
	dwt_write32bitreg(TX_POWER_ID, txpower);
	return DWT_SUCCESS;
}

static int
dw1000_set_promiscuous_mode(struct ieee802154_hw *hw, const bool on)
{
	uint32_t filcfg;
	if (on)
		filcfg = DWT_FF_NOTYPE_EN;
	else
	{
		filcfg = DWT_FF_BEACON_EN
			   | DWT_FF_DATA_EN
			   | DWT_FF_ACK_EN 
			   | DWT_FF_MAC_EN 
			   | DWT_FF_RSVD_EN;
	}
	dwt_enableframefilter(filcfg);
	return DWT_SUCCESS;
}

static int
dw1000_hwts_set(struct ieee802154_hw *hw, struct ifreq *ifr)
{
	struct dw1000_private *priv = hw->priv;
	struct hwtstamp_config cfg;
	if (copy_from_user(&cfg, ifr->ifr_data, sizeof(cfg)))
		return -EFAULT;
	if (cfg.flags)
		return -EINVAL;
	switch (cfg.tx_type) {
	case HWTSTAMP_TX_ON: 
	case HWTSTAMP_TX_OFF: 
		priv->hwts_tx_state = (cfg.tx_type > 0);
		break;
	case HWTSTAMP_TX_ONESTEP_SYNC:
	default:
		return -ERANGE;
	}
	switch (cfg.rx_filter) {
	case HWTSTAMP_FILTER_NONE:
	case HWTSTAMP_FILTER_ALL:
	case HWTSTAMP_FILTER_SOME:
	case HWTSTAMP_FILTER_PTP_V1_L4_EVENT:
	case HWTSTAMP_FILTER_PTP_V1_L4_SYNC:
	case HWTSTAMP_FILTER_PTP_V1_L4_DELAY_REQ:
	case HWTSTAMP_FILTER_PTP_V2_L4_EVENT:
	case HWTSTAMP_FILTER_PTP_V2_L4_SYNC:
	case HWTSTAMP_FILTER_PTP_V2_L4_DELAY_REQ:
	case HWTSTAMP_FILTER_PTP_V2_L2_EVENT:
	case HWTSTAMP_FILTER_PTP_V2_L2_SYNC:
	case HWTSTAMP_FILTER_PTP_V2_L2_DELAY_REQ:
	case HWTSTAMP_FILTER_PTP_V2_EVENT:
	case HWTSTAMP_FILTER_PTP_V2_SYNC:
	case HWTSTAMP_FILTER_PTP_V2_DELAY_REQ:
		priv->hwts_rx_state = (cfg.rx_filter > 0);
		break;
	default:
		return -ERANGE;
	}
	return copy_to_user(ifr->ifr_data, &cfg, sizeof(cfg)) ? -EFAULT : 0;
}

static int
dw1000_hwts_get(struct ieee802154_hw *hw, struct ifreq *ifr)
{
	struct dw1000_private *priv = hw->priv;
	struct hwtstamp_config cfg;
	cfg.flags = 0;
	cfg.tx_type = priv->hwts_tx_state;
	cfg.rx_filter = priv->hwts_rx_state;
	return copy_to_user(ifr->ifr_data, &cfg, sizeof(cfg)) ? -EFAULT : 0;
}

static int
dw1000_hwts_info(struct ieee802154_hw *hw, struct ethtool_ts_info *info)
{
	struct dw1000_private *priv = hw->priv;
	info->so_timestamping = SOF_TIMESTAMPING_SOFTWARE
						  | SOF_TIMESTAMPING_TX_SOFTWARE
						  | SOF_TIMESTAMPING_RX_SOFTWARE
						  | SOF_TIMESTAMPING_RX_HARDWARE
						  | SOF_TIMESTAMPING_TX_HARDWARE
						  | SOF_TIMESTAMPING_RAW_HARDWARE;
	info->phc_index = priv->phc_index;
	info->tx_types =
		  (1 << HWTSTAMP_TX_OFF)
		| (1 << HWTSTAMP_TX_ON);
	info->rx_filters = 
		  (1 << HWTSTAMP_FILTER_NONE)
		| (1 << HWTSTAMP_FILTER_ALL)
		| (1 << HWTSTAMP_FILTER_SOME)
		| (1 << HWTSTAMP_FILTER_PTP_V1_L4_EVENT)
		| (1 << HWTSTAMP_FILTER_PTP_V1_L4_SYNC)
		| (1 << HWTSTAMP_FILTER_PTP_V1_L4_DELAY_REQ)
		| (1 << HWTSTAMP_FILTER_PTP_V2_L4_EVENT)
		| (1 << HWTSTAMP_FILTER_PTP_V2_L4_SYNC)
		| (1 << HWTSTAMP_FILTER_PTP_V2_L4_DELAY_REQ)
		| (1 << HWTSTAMP_FILTER_PTP_V2_L2_EVENT)
		| (1 << HWTSTAMP_FILTER_PTP_V2_L2_SYNC)
		| (1 << HWTSTAMP_FILTER_PTP_V2_L2_DELAY_REQ)
		| (1 << HWTSTAMP_FILTER_PTP_V2_EVENT)
		| (1 << HWTSTAMP_FILTER_PTP_V2_SYNC)
		| (1 << HWTSTAMP_FILTER_PTP_V2_DELAY_REQ);
	return 0;
}

static const struct ieee802154_ops dw1000_ops = {
	.owner = THIS_MODULE,
	.start = dw1000_start,
	.stop = dw1000_stop,
	.xmit_async = dw1000_tx,
	.ed = dw1000_ed,
	.set_channel = dw1000_set_channel,
	.set_hw_addr_filt = dw1000_filter,
	.set_txpower = dw1000_txpower,
	.set_promiscuous_mode = dw1000_set_promiscuous_mode,
	.hwts_set = dw1000_hwts_set,
	.hwts_get = dw1000_hwts_get,
	.hwts_info = dw1000_hwts_info,
};

// IEEE 802.15.4 hardware registration /////////////////////////////////////////

static int 
dw1000_register(struct dw1000_private *priv)
{
	int ret = -ENOMEM;

	priv->hw = ieee802154_alloc_hw(sizeof(*priv), &dw1000_ops);
	if (!priv->hw)
		goto err_ret;

	priv->hw->priv = priv;
	priv->hw->parent = &priv->spi->dev;
	priv->hw->extra_tx_headroom = 0;
	priv->hw->vif_data_size = sizeof(*priv);
	ieee802154_random_extended_addr(&priv->hw->phy->perm_extended_addr);

	// UWB preamble sense based on the SHR of a frame
	priv->hw->phy->cca.mode = NL802154_CCA_UWB_SHR;

	/* We do support only 2.4 Ghz */
	priv->hw->phy->channels_supported[4] = 0xBE;	// channel mask
	priv->hw->phy->current_page = 4;				// default page
	priv->hw->phy->current_channel = 7;				// default channe;
	priv->hw->flags = IEEE802154_HW_TX_OMIT_CKSUM 	// radio calculates checksum
				    | IEEE802154_HW_TXPOWER 		// tx power can be changed
				    | IEEE802154_HW_AFILT			// Address filtering
				    | IEEE802154_HW_TIMESTAMPS		// Hardware timestamping
					| IEEE802154_HW_PROMISCUOUS;	// Promiscuous mode

	// Create a wpanX interface
	ret = ieee802154_register_hw(priv->hw);
	if (ret)
		goto err_free_device;
	return 0;

err_free_device:
	ieee802154_free_hw(priv->hw);
err_ret:
	return ret;
}

static int 
dw1000_initialize(uint16_t panId, uint16_t shortAddr)
{
	dwt_config_t ch_config;
	dwt_txconfig_t tx_calib;

	// initialization result
	int32_t result, otp_options;
	uint8_t local_panId[2];
	uint8_t local_shortAddress[2];
	uint16_t antennadelay;

	// default channel configuration (taken from mode 3 of EVK1000)
	ch_config.chan = 7;
	ch_config.rxCode = 17;
	ch_config.txCode = 17;
	ch_config.prf = DWT_PRF_64M;
	ch_config.dataRate = DWT_BR_6M8;
	ch_config.txPreambLength = DWT_PLEN_128;
	ch_config.rxPAC = DWT_PAC8;
	ch_config.nsSFD = 0;
	ch_config.phrMode = DWT_PHRMODE_STD;
	ch_config.sfdTO = (256+64-32);
	ch_config.smartPowerEn = 0;
	tx_calib.PGdly = TC_PGDELAY_CH7;
	tx_calib.power = 0x00000000;

	// copy network IDs
	local_panId[0] = (uint8_t)( panId  & 0xFF);
	local_panId[1] = (uint8_t)((panId >> 8) & 0xFF);
	local_shortAddress[0] = (uint8_t)( shortAddr       & 0xFF);
	local_shortAddress[1] = (uint8_t)((shortAddr >> 8) & 0xFF);

	// wire up the SPI and sleep functions to the DW driver
	dwt_wire_spifcns(&dw1000_write, &dw1000_read);
	dwt_wire_sleepfcns(&dw1000_sleep_ms);

	// reset the DW1000
	dwt_softreset();

	// One-time-programmable (OTP) memory loading
	otp_options = DWT_LOADUCODE | DWT_LOADLDO | DWT_LOADXTALTRIM;

	// load from OTP and initialize DW1000
	result = dwt_initialise(otp_options);
	if (result != DWT_SUCCESS) 
		return DWR_RETURN_ERR;

	// configure the channel
	dwt_configure(&ch_config, otp_options);
	dwt_configuretxrf(&tx_calib);

    // antenna delay calibration
    antennadelay = (uint16_t)( (DWT_PRF_64M_RFDLY/2.0)*1e-9/DWT_TIME_UNITS);
    dwt_setrxantennadelay(antennadelay);
    dwt_settxantennadelay(antennadelay);

    // configure RX mode. Only low power (don't listen) should have RX timeout
    dwt_setrxmode(DWT_RX_NORMAL, 0, 0);
    dwt_setautorxreenable(1);

    // always receive
	dwt_setrxtimeout(0);

	// Promiscuous mode by default
	dwt_enableframefilter(DWT_FF_NOTYPE_EN);
	
	// only allow interrupts on good rx frames and good tx frames
	dwt_setinterrupt(DWT_INT_RFCG | DWT_INT_TFRS, 1);

	// set PAN and short address
	dwt_setpanid(panId);
	dwt_setaddress16(shortAddr);

	// Initialization complete
	return DWT_SUCCESS;
}

static enum hrtimer_restart 
dw1000_overflow(struct hrtimer *timer)
{
	struct timespec64 ts;
	struct dw1000_private *ctx = container_of(timer, struct dw1000_private, htimer);
	dw1000_ptp_gettime(&ctx->info, &ts);
	//pr_info("dw1000 overflow check at %lld.%09lu\n", ts.tv_sec, ts.tv_nsec);
	hrtimer_forward_now(timer, ctx->period);
    return HRTIMER_RESTART;
}

// IRQ HANDLING ////////////////////////////////////////////////////////////////

static void 
dw1000_async_rx(void *context)
{
	struct dw1000_async *ctx = context;
	struct sk_buff *skb;
	struct skb_shared_hwtstamps *sshptr;
	uint16_t len, lqi;

	// Get link quality
	len = dwt_read16bitoffsetreg(RX_FINFO_ID, 0) & 0x3FF;
	lqi = dwt_read16bitoffsetreg(RX_FQUAL_ID, 0);

	// Add checksum!
	len += 2;

	// Allocate a socket buffer to pass packet to the MAC
	skb = dev_alloc_skb(len);
	if (skb)
	{
		// Read the data into the socket buffer
		if (dwt_readfromdevice(RX_BUFFER_ID, 0, len, skb_put(skb, len)))
		{
			pr_info("dw1000_async_rx: frame reception failed\n");
			kfree_skb(skb);
		}
		else
		{
			// Remove the checksum
			skb_trim(skb, skb->len - 2);

			// Timestamp, if needed
			if (ctx->lp->hwts_rx_state)
			{
				uint8_t  tr[5];
				uint64_t ts;
				dwt_readrxtimestamp(tr);
				ts 	= ((uint64_t)tr[4] << 32) 
					+ ((uint64_t)tr[3] << 24) 
					+ ((uint64_t)tr[2] << 16) 
					+ ((uint64_t)tr[1] <<  8) 
				    + ((uint64_t)tr[0]);
				ts = (ts >> DW1000_SHIFT);
				sshptr = skb_hwtstamps(skb);
				memset(sshptr, 0, sizeof(*sshptr));
				sshptr->hwtstamp = ns_to_ktime(timecounter_cyc2time(&ctx->lp->tc, ts));
				pr_info("dw1000 RX Timestamp %lld\n", sshptr->hwtstamp.tv64); // FATIMA DEBUG
			}
			ieee802154_rx_irqsafe(ctx->lp->hw, skb, lqi);
		}
	}
	else
		pr_info("dw1000_async_rx: could not alloc skb\n");

	// Switch back into receive mode
	dwt_rxenable(0);
}

static void
dw1000_async_tx(void *context)
{
	struct dw1000_async *ctx = context;
	struct skb_shared_hwtstamps ssh;
	
	if (ctx->lp->hwts_tx_state)
	{
		uint8_t  tr[5];
		uint64_t ts;
		if (skb_shinfo(ctx->lp->skb)->tx_flags & SKBTX_IN_PROGRESS)
		{
			dwt_readtxtimestamp(tr);
			ts 	= ((uint64_t)tr[4] << 32) 
				+ ((uint64_t)tr[3] << 24) 
				+ ((uint64_t)tr[2] << 16) 
				+ ((uint64_t)tr[1] <<  8) 
			    + ((uint64_t)tr[0]);
			ts = (ts >> DW1000_SHIFT);
			memset(&ssh, 0, sizeof(ssh));
			ssh.hwtstamp = ns_to_ktime(timecounter_cyc2time(&ctx->lp->tc, ts));
			skb_tstamp_tx(ctx->lp->skb, &ssh);
			pr_info("dw1000 TX Timestamp %lld\n", ssh.hwtstamp.tv64); // FATIMA DEBUG
		}
	}
	ieee802154_xmit_complete(ctx->lp->hw, ctx->lp->skb, true);
}

static void 
dw1000_async_recover(void *context)
{
	dwt_forcetrxoff();
	dwt_rxreset();
	dwt_rxenable(0);
}

static void 
dw1000_irq_status(void *context)
{
	// Get the context of the SPI call
	struct dw1000_async *ctx = context;
	uint32_t status, bitsToClear; 
	int j;

    // Read the IRQ status from the SPI async response
    status = 0;
    if (ctx->msg.status == DWT_SUCCESS)
        for (j = 3 ; j >= 0 ; j --)
            status = (status << 8) + ctx->body[j];
    else
    	pr_info("dw1000_irq_status: Problem reading IRQ\n");
    bitsToClear = 0x00;

	// Receiver FCS Good
    if (status & SYS_STATUS_RXFCG) 
	{
		bitsToClear |= status & CLEAR_ALLRXGOOD_EVENTS;
		if (status & SYS_STATUS_RXOVRR) 
			dw1000_async_recover(ctx);
		else
			dw1000_async_rx(ctx);
    }
    // Frame transmit good
    else if (status & SYS_STATUS_TXFRS)
    {
		bitsToClear |= CLEAR_ALLTX_EVENTS;
		dw1000_async_tx(ctx);
    }
    else if (status & SYS_STATUS_RXRFTO)
    {
        bitsToClear |= status & SYS_STATUS_RXRFTO;     
    }
    else if (status & CLEAR_ALLRXERROR_EVENTS)
    {
        bitsToClear |= status & CLEAR_ALLRXERROR_EVENTS;
		dw1000_async_recover(ctx);
    }

    // Clear IRQ bits
    dwt_write32bitreg(SYS_STATUS_ID, bitsToClear);    

    // Enable IRQ again!
	enable_irq(ctx->irq);
}

static irqreturn_t 
dw1000_isr(int irq, void *data)
{
	int rc;
	struct dw1000_private *lp = data;
	struct dw1000_async *ctx = &lp->state;

	// Disable IRQ until we have read the state
	disable_irq_nosync(irq);

	// Save the current IRQ state and register a callback
	ctx->msg.complete = dw1000_irq_status;
	rc = dw1000_readfromdevice(ctx, SYS_STATUS_ID, 0, 4); 
	if (rc) {
		enable_irq(irq);
		pr_info("dw1000_isr: async error\n");
		return IRQ_NONE;
	}
	return IRQ_HANDLED;
}


// DEVICE TREE /////////////////////////////////////////////////////////////////

static int 
dw1000_get_platform_data(struct spi_device *spi, struct dw1000_platform_data *pdata)
{
	struct device_node *np = spi->dev.of_node;
	if (!np) {
		struct dw1000_platform_data *spi_pdata = spi->dev.platform_data;
		if (!spi_pdata)
			return -ENOENT;
		*pdata = *spi_pdata;
		return DWT_SUCCESS;
	}
	pdata->reset = of_get_named_gpio(np, "reset-gpio", 0);
	return DWT_SUCCESS;
}

// MODULE LOAD AND UNLOAD ./////////////////////////////////////////////////////

static int 
dw1000_probe(struct spi_device *spi)
{
	struct dw1000_platform_data pdata;
	int ret;

	priv = devm_kzalloc(&spi->dev, sizeof(*priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	spi_set_drvdata(spi, priv);
	ret = dw1000_get_platform_data(spi, &pdata);
	if (ret < 0) {
		dev_err(&spi->dev, "no platform data\n");
		return -EINVAL;
	}
	priv->spi = spi;				

	// Set timestamping off by default
	priv->hwts_rx_state = HWTSTAMP_FILTER_NONE;
	priv->hwts_tx_state = HWTSTAMP_TX_OFF;

	// Request all the gpio's 
	if (!gpio_is_valid(pdata.reset)) {
		dev_err(&spi->dev, "reset gpio is not valid\n");
		ret = -EINVAL;
		goto err_hw_init;
	}

	// Reset 
	ret = devm_gpio_request_one(&spi->dev, pdata.reset,
		GPIOF_OUT_INIT_LOW, "reset");
	usleep_range(100, 200);
	gpio_set_value(pdata.reset, 1);
	usleep_range(100, 200);

	// Set up the asynchronous SPI message structures
	dw1000_spi_register(priv);

	// Initialize hardware 
	priv->speed = SPI_SPEED_SLOW;
	ret = dw1000_initialize(0xAE70, 0x0002);
	if (ret)
		goto err_hw_init;
	priv->speed = SPI_SPEED_FAST;

	// Check periodically for overflows
    hrtimer_init (&priv->htimer, CLOCK_REALTIME, HRTIMER_MODE_REL);
	priv->basetime = 0;
	priv->lasttime = 0;
    priv->htimer.function = dw1000_overflow;
    priv->period = ktime_set(DWR_OVERFLOW_CHECK_SEC,0);
    hrtimer_start(&priv->htimer, priv->period, HRTIMER_MODE_REL);

	// Setup IRQ 
	ret = devm_request_irq(&spi->dev, spi->irq, dw1000_isr,
     	IRQF_TRIGGER_RISING, dev_name(&spi->dev), priv);
	if (ret)
		goto err_hw_init;

	// Register 802.15.4 interface 
	ret = dw1000_register(priv);
	if (ret)
		goto err_hw_init;

	// Register PTP clock (with a given multiplier and shift)
	ret = dw1000_ptp_register(priv);
	if (ret)
		goto err_hw_init;

	// Success!
	return DWT_SUCCESS;

err_hw_init:
	return ret;
}

static int 
dw1000_remove(struct spi_device *spi)
{
	// Register PTP clock
	struct dw1000_private *priv = spi_get_drvdata(spi);
	dw1000_ptp_unregister(priv);
	ieee802154_unregister_hw(priv->hw);
	ieee802154_free_hw(priv->hw);
	hrtimer_cancel(&priv->htimer);
	dw1000_spi_unregister(priv);
	return DWT_SUCCESS;
}

static const struct spi_device_id dw1000_ids[] = {
	{"dw1000", },
	{},
};
MODULE_DEVICE_TABLE(spi, dw1000_ids);

static const struct of_device_id dw1000_of_ids[] = {
	{.compatible = "decawave,dw1000", },
	{},
};
MODULE_DEVICE_TABLE(of, dw1000_of_ids);

/* SPI driver structure */
static struct spi_driver dw1000_driver = {
	.driver = {
		.name = "dw1000",
		.bus = &spi_bus_type,
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(dw1000_of_ids),
	},
	.id_table = dw1000_ids,
	.probe = dw1000_probe,
	.remove = dw1000_remove,
};
module_spi_driver(dw1000_driver);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Andrew Symington <andrew.c.symington@gmail.com>");
MODULE_DESCRIPTION("DW1000 Transceiver Driver");
MODULE_VERSION("0.1.0");
